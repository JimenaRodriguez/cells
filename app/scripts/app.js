(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'cards': '/cards',
      'detail': '/detail'
    }
  });

}(document));
